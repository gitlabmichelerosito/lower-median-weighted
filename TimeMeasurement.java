public class TimeMeasurement 
{
    public static double systemGranularity()
    {
        double t0 = System.currentTimeMillis();
        double t1 = System.currentTimeMillis();
        
        while ( t1 == t0 )
        {
            t1 = System.currentTimeMillis();
        }
        return( t1 - t0 );
    }

    public static double measurement( int dim, double tMin )
    {
        double    t = 0;
        double sum2 = 0;
        int      cn = 0;
        
        double e;
        double delta;
        do
        {
            for ( int i = 1; i <= 10; i++ )
            {
                double m = averageTimeCalculation( dim, tMin );
                
                t    = t + m;
                sum2 = sum2 + ( m * m );
            }
            cn = cn + 10;
            e  = t / cn;
            
            double s = Math.sqrt( sum2 / cn - ( e * e ) );
           
            delta = ( 1 / Math.sqrt( cn ) ) * 1.96 * s;

        } while( delta < e / 10 );
        
        return( e );
    }

    public static double averageTimeCalculation( int dim, double tMin )
    {
        int ripTara  = tareRepetitionCalculation( dim, tMin );
        int ripLordo = grossRepetitionCalculation( dim, tMin );
        
        double t0 = System.currentTimeMillis();

        for ( int i = 0; i <= ripTara; i++ )
        {
            double seed = System.currentTimeMillis();
            double[] randomSequence = prngSequence( seed, dim );
        }
        double t1    = System.currentTimeMillis();
        double tTara = t1 - t0;
        
        t0 = System.currentTimeMillis();
        
        for ( int i = 0; i <= ripLordo; i++ )
        {
            double seed = System.currentTimeMillis();
            double[] randomSequence  = prngSequence( seed, dim );
            
            double target = summation( randomSequence ) / 2;
            lowerMedianWeighted( randomSequence, 0, target );
        }
        t1 = System.currentTimeMillis();
        
        double tLordo = t1 - t0;
        double tMedio = tLordo / ripLordo - tTara / ripTara;
        
        return( tMedio );
    }

    public static int grossRepetitionCalculation( int dim, double tMin )
    {
        double t0 = 0;
        double t1 = 0;
        int rip   = 1;
        
        while ( t1 - t0 <= tMin )
        {
            rip = rip * 2;
            t0  = System.currentTimeMillis();
            
            for ( int i = 0; i <= rip; i++ )
            {
                double seed = System.currentTimeMillis();
                double[] randomSequence = prngSequence( seed, dim );
                double sumDiv2 = summation( randomSequence ) / 2;
                
                lowerMedianWeighted( randomSequence, 0, sumDiv2 );
            }
            t1 = System.currentTimeMillis();
        }

        int max = rip;
        int min = rip / 2;
        int cicliErrati = 5;

        while ( max - min >= cicliErrati )
        {
            rip = ( max - min ) / 2;
            t0  = System.currentTimeMillis();
            for ( int i = 1; i <= rip; i++ )
            {
                double seed = System.currentTimeMillis();
                double[] randomSequence = prngSequence( seed, dim );
                double sumDiv2 = summation( randomSequence ) / 2;
                
                lowerMedianWeighted( randomSequence, 0, sumDiv2 );
            }
            t1 = System.currentTimeMillis();
            
            if ( t1 - t0 <= tMin )
                min = rip;
            else
                max = rip;
        }

        return( max );
    }

    public static int tareRepetitionCalculation( int dim, double tMin )
    {
        double t0 = 0;
        double t1 = 0;
        int rip   = 1;

        while ( t1 - t0 <= tMin )
        {
            rip = rip * 2;
            t0  = System.currentTimeMillis();
            
            for ( int i = 0; i <= rip; i++ )
            {
                double seed = System.currentTimeMillis();
                double[] randomSequence = prngSequence( seed, dim );
                double sumDiv2 = summation( randomSequence ) / 2;
            }
            t1 = System.currentTimeMillis();
        }

        int max = rip;
        int min = rip / 2;
        int cicliErrati = 5;

        while ( max - min >= cicliErrati )
        {
            rip = ( max - min ) / 2;
            t0 = System.currentTimeMillis();
            for ( int i = 1; i <= rip; i++ )
            {
                double seed = System.currentTimeMillis();
                double[] randomSequence = prngSequence( seed, dim );
                double sumDiv2 = summation( randomSequence ) / 2;
            }
            t1 = System.currentTimeMillis();
            if ( t1 - t0 <= tMin )
                min = rip;
            else
                max = rip;
        }
        return max;
    }

    public static double[] prngSequence( double seed, int howManyNumbersGenerate ) 
    {   
        double[] randomSequence = new double[ howManyNumbersGenerate ];
        
        for ( int i = 0; i < howManyNumbersGenerate; i++ ) 
        {
            double nextNumber = Math.pow( 7, 5 ) * seed % ( Math.pow( 2, 31 ) - 1 ); 
            seed = nextNumber;
            double singleResult = ( 2 * ( seed / ( Math.pow( 2, 31 ) - 2 ) ) ); // singleResult in [0,1) 
            randomSequence[i] = singleResult;
        }
        return( randomSequence );
    }

    /*  MAIN PROGRAM  */
    public static void main( String args[] )
    {
        int    dim = 1048576;
        
        double tMin = systemGranularity();
        System.out.println( "CALCOLO TMIN... COMPLETATO." );
        
        double time = measurement( dim, tMin );
        System.out.println( "CALCOLO TEMPO_MEDIO_NETTO... COMPLETATO." );

        System.out.println( "Per dimensione pari a " + dim + " il tempo è" );
        System.out.println( time );
    }

    /*  PROBLEM-SOLVER'S FUNCTIONS  */

    private static void lowerMedianWeighted( double[] sequence, double leftSum, double sumDiv2 )
    {
        double median = select( sequence, sequence.length ); 
        
        lomutoPartition( sequence, median );
        
        double actualLeftSum = 0;        
        for ( double v : sequence )
        {
            if ( v <= median )
                actualLeftSum += v;
        }

        actualLeftSum -= median;
        int index = indexMedianSeach( sequence, median );

        if ( leftSum + actualLeftSum < sumDiv2 && leftSum + actualLeftSum + median >= sumDiv2 )
            System.out.print( "" );
        else if ( leftSum + actualLeftSum + median < sumDiv2 && leftSum + actualLeftSum + median < sumDiv2 )
        {
            leftSum += actualLeftSum + median;
            
            double[] newSequence = new double[ sequence.length - index - 1 ];
            for ( int i = 0; i < newSequence.length; i++ )
            {
                newSequence[i] = sequence[ index + 1 + i ];
            }

            lowerMedianWeighted( newSequence, leftSum, sumDiv2 );
        }
        else 
        {
            double[] newSequence = new double[ index ];
            for ( int i = 0; i < newSequence.length; i++ )
            {
                newSequence[i] = sequence[i];
            }

            lowerMedianWeighted( newSequence, leftSum, sumDiv2 );
        }
    }

    private static double summation( double[] sequence )
    {
        double result = 0;
        for ( double v : sequence )
        {
            result = result + v;     
        }

        return( result );
    }
    
    private static double select( double[] sequence, int dim )
    {
        if ( sequence.length < 6 )
        {
            selectionSort( sequence );
            return sequence[ dim / 2 ];
        }
        else 
        {
            int nMedians;
            int blocks = dim / 5;
            int odds   = dim % 5;
            
            if ( odds == 0 )    
                nMedians = blocks;
            else
                nMedians = blocks + odds;
            
            double[] mediansSequence = new double[ nMedians ]; 

            for ( int i = 0; i <= blocks; i++ ) 
            {
                int lowIndex  = i * 5;        //bounds for subarrays where to search median
                int highIndex = lowIndex + 4;
                if ( highIndex >= dim )       
                    highIndex = dim - 1;
                if ( lowIndex <= dim - 1 )
                    mediansSequence[i] = medianSearch( sequence, lowIndex, highIndex ); 
            }
            
            int oddsIndex = blocks * 5 + 1;
            for ( int i = blocks + 1; i < mediansSequence.length; i++ )
            {
                mediansSequence[i] = sequence[ oddsIndex ];
                oddsIndex++;
            }

            double pivot = select( mediansSequence, nMedians );
            
            lomutoPartition( mediansSequence, pivot );
            
            return( mediansSequence[ nMedians / 2 ] );
        }
    }

    private static double medianSearch( double[] sequence, int lowIndex, int highIndex )
    {
        double[] newSequence = new double[ highIndex - lowIndex + 1 ];
        for ( int i = 0; i < newSequence.length; i++ ) 
        {
            newSequence[i] = sequence[ lowIndex ];
            lowIndex++;
        }

        selectionSort( newSequence );    
        
        return( newSequence[ newSequence.length / 2 ] );
    }

    private static void selectionSort( double[] sequence ) 
    {
        for ( int i = 0; i < sequence.length - 1; i++ ) 
        {       
            for ( int j = i + 1; j < sequence.length; j++ )
            {
                if ( sequence[j] < sequence[i] ) 
                {
                    double temp = sequence[j];
                    sequence[j]  = sequence[i];
                    sequence[i]  = temp;
                }
            }
        }
    }
    
    private static void lomutoPartition( double[] sequence, double pivot )
    {
        int j = -1;
        double temp;

        for ( int i = 0; i < sequence.length; i++ ) 
        {     
            if ( sequence[i] == pivot )
            {                 
                temp = sequence[ sequence.length - 1 ] ;
                sequence[ sequence.length - 1 ] = sequence[i];
                sequence[i] = temp;
            }
            if ( sequence[i] <= pivot )
            {                        
                j++;                 
                temp     = sequence[i]; 
                sequence[i] = sequence[j]; 
                sequence[j] = temp;     
            }
        }
    }
   
    private static int indexMedianSeach( double[] sequence, double median )
    {
        int index = sequence.length - 1;         
        while ( sequence[ index ] != median )
        {
            index--;
        }

        return( index );
    }

    /*  PARSING FUNCTION  */
    private static void parseArray( String[] inputSequence, double[] parsedSequence )
    {
        int j = 0;      

        for ( int i = 0; i < parsedSequence.length; i++ ) 
        {
            parsedSequence[i] = -1;
        }

        for ( int i = 0 ; i < inputSequence.length - 1; i++ ) 
        {                              
            String clear = inputSequence[i].replaceAll( "[^0-9.]", "" );  
            if ( clear.length() > 0 )
            {
                j++;
                parsedSequence[i] = Double.parseDouble( clear );
            }
        }

        double[] finalSequence = new double[j];
        for ( int i = 0; i < j; i++ )
        {
            finalSequence[i] = parsedSequence[i];
        }
    }
}