public class LowerMedianWeighted 
{
    /*  PROBLEM-SOLVER'S FUNCTIONS  */

    private static void lowerMedianWeighted( double[] sequence, double leftSum, double sumDiv2 )
    {
        double median = select( sequence, sequence.length ); 
        
        lomutoPartition( sequence, median );
        
        double actualLeftSum = 0;        
        for ( double v : sequence )
        {
            if ( v <= median )
                actualLeftSum += v;
        }

        actualLeftSum -= median;
        int index = indexMedianSeach( sequence, median );

        if ( leftSum + actualLeftSum < sumDiv2 && leftSum + actualLeftSum + median >= sumDiv2 )
            System.out.print( sequence[ index ] );
        else if ( leftSum + actualLeftSum + median < sumDiv2 && leftSum + actualLeftSum + median < sumDiv2 )
        {
            leftSum += actualLeftSum + median;
            
            double[] newSequence = new double[ sequence.length - index - 1 ];
            for ( int i = 0; i < newSequence.length; i++ )
            {
                newSequence[i] = sequence[ index + 1 + i ];
            }

            lowerMedianWeighted( newSequence, leftSum, sumDiv2 );
        }
        else 
        {
            double[] newSequence = new double[ index ];
            for ( int i = 0; i < newSequence.length; i++ )
            {
                newSequence[i] = sequence[i];
            }

            lowerMedianWeighted( newSequence, leftSum, sumDiv2 );
        }
    }

    private static double summation( double[] sequence )
    {
        double result = 0;
        for ( double v : sequence )
        {
            result = result + v;     
        }

        return( result );
    }
    
    private static double select( double[] sequence, int dim )
    {
        if ( sequence.length < 6 )
        {
            selectionSort( sequence );
            return sequence[ dim / 2 ];
        }
        else 
        {
            int nMedians;
            int blocks = dim / 5;
            int odds   = dim % 5;
            
            if ( odds == 0 )    
                nMedians = blocks;
            else
                nMedians = blocks + odds;
            
            double[] mediansSequence = new double[ nMedians ]; 

            for ( int i = 0; i <= blocks; i++ ) 
            {
                int lowIndex  = i * 5;        
                int highIndex = lowIndex + 4;
                if ( highIndex >= dim )       
                    highIndex = dim - 1;
                if ( lowIndex <= dim - 1 )
                    mediansSequence[i] = medianSearch( sequence, lowIndex, highIndex ); 
            }
            
            int oddsIndex = blocks * 5 + 1;
            for ( int i = blocks + 1; i < mediansSequence.length; i++ )
            {
                mediansSequence[i] = sequence[ oddsIndex ];
                oddsIndex++;
            }

            double pivot = select( mediansSequence, nMedians );
            
            lomutoPartition( mediansSequence, pivot );
            
            return( mediansSequence[ nMedians / 2 ] );
        }
    }

    private static double medianSearch( double[] sequence, int lowIndex, int highIndex )
    {
        double[] newSequence = new double[ highIndex - lowIndex + 1 ];
        for ( int i = 0; i < newSequence.length; i++ ) 
        {
            newSequence[i] = sequence[ lowIndex ];
            lowIndex++;
        }

        selectionSort( newSequence );    
        
        return( newSequence[ newSequence.length / 2 ] );
    }

    private static void selectionSort( double[] sequence ) 
    {
        for ( int i = 0; i < sequence.length - 1; i++ ) 
        {       
            for ( int j = i + 1; j < sequence.length; j++ )
            {
                if ( sequence[j] < sequence[i] ) 
                {
                    double temp = sequence[j];
                    sequence[j]  = sequence[i];
                    sequence[i]  = temp;
                }
            }
        }
    }
    
    private static void lomutoPartition( double[] sequence, double pivot )
    {
        int j = -1;
        double temp;

        for ( int i = 0; i < sequence.length; i++ ) 
        {     
            if ( sequence[i] == pivot )
            {                 
                temp = sequence[ sequence.length - 1 ] ;
                sequence[ sequence.length - 1 ] = sequence[i];
                sequence[i] = temp;
            }
            if ( sequence[i] <= pivot )
            {                        
                j++;                 
                temp     = sequence[i]; 
                sequence[i] = sequence[j]; 
                sequence[j] = temp;     
            }
        }
    }
   
    private static int indexMedianSeach( double[] sequence, double median )
    {
        int index = sequence.length - 1;         
        while ( sequence[ index ] != median )
        {
            index--;
        }

        return( index );
    }

    /*  PARSING FUNCTION  */
    private static double[] parseSequence( String input ) 
    {
        double[] sequence;

        String[] formattedInput = input.split(","); //New array of double using ',' 
        
        int last = formattedInput.length - 1;
        formattedInput[ last ] = formattedInput[ last ].substring( 0, formattedInput[ last ].length() - 1);
        
        sequence = new double[ formattedInput.length ];
        for ( int i = 0; i < formattedInput.length; i++ ) 
        {
            sequence[i] = Double.parseDouble( formattedInput[i] );
        }

        return( sequence );
    }

    /*  MAIN PROGRAM  */
    public static void main( String[] args )
    {
        int ch;
        String stringOfDoubles = "";                   
        
        try 
        {
            ch = System.in.read();
            while ( ch != -1 ) 
            {
                stringOfDoubles = stringOfDoubles + (char)ch;
                ch = System.in.read();
            }
        }
        catch( Exception error )
        {
            System.out.println( error.getMessage() );
        }

        double[] sequence;

        stringOfDoubles = stringOfDoubles.trim();

        if ( stringOfDoubles.length() == 0 || stringOfDoubles.equals( "." ) ) 
        {
            System.out.print( "" );
        }
        else 
        {
            sequence = parseSequence( stringOfDoubles );

            double sumDiv2 = summation( sequence ) / 2;                   
            
            lowerMedianWeighted( sequence, 0, sumDiv2 );
        }
    }    
}